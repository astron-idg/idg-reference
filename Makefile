CXX=g++
CXXFLAGS=-std=c++11 -O3 -fopenmp

default: main.x

main.x: main.o auxiliary/Init.o auxiliary/Plan.o auxiliary/uvwsim.o kernel.o
	$(CXX) $(CXXFLAGS) -I./auxiliary $^ -o $@

kernel.o: kernel.cpp
	$(CXX) $(CXXFLAGS) -I./auxiliary -c $< -o $@ -march=native -ffast-math

%.o: %.cpp
	$(CXX) $(CXXFLAGS) -I./auxiliary -c $< -o $@

clean:
	@rm -f *.x *.o \
	@rm -f auxiliary/*.o
