/*******************************************************************************
 *
 * main.cpp
 *
 * Copyright (C) 2017
 * ASTRON (Netherlands Institute for Radio Astronomy)
 * P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
 *
 * This file is part of the Image Domain Gridding (IDG) suite.
 * The  IDG suite is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * The IDG suite is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with the IDG suite. If not, see <http://www.gnu.org/licenses/>.
 *
 ******************************************************************************/

#include "Types.h"
#include "Plan.h"
#include "Init.h"

#include "kernel.h"

uint64_t flops_gridder(
    uint64_t nr_channels,
    uint64_t nr_timesteps,
    uint64_t nr_subgrids)
{
    uint64_t subgrid_size = SUBGRID_SIZE;
    uint64_t nr_correlations = NR_CORRELATIONS;

    // Number of flops per visibility
    uint64_t flops_per_visibility = 0;
    flops_per_visibility += 5; // phase index
    flops_per_visibility += 5; // phase offset
    flops_per_visibility += nr_channels * 2; // phase
    flops_per_visibility += nr_channels * nr_correlations * 8; // update

    // Number of flops per subgrid
    uint64_t flops_per_subgrid = 0;
    flops_per_subgrid += nr_correlations * 30; // aterm
    flops_per_subgrid += nr_correlations * 2; // spheroidal
    flops_per_subgrid += 6; // shift

    // Total number of flops
    uint64_t flops_total = 0;
    flops_total += nr_timesteps * subgrid_size * subgrid_size * flops_per_visibility;
    flops_total += nr_subgrids  * subgrid_size * subgrid_size * flops_per_subgrid;
    return flops_total;
}

int main(int argc, char **argv)
{
    // Constants
    unsigned int nr_correlations  = NR_CORRELATIONS;
    float w_offset                = W_OFFSET;
    unsigned int nr_stations      = NR_STATIONS;
    unsigned int nr_channels      = NR_CHANNELS;
    unsigned int nr_timesteps     = NR_TIMESTEPS;
    unsigned int nr_timeslots     = NR_TIMESLOTS;
    float image_size              = IMAGE_SIZE;
    unsigned int grid_size        = GRID_SIZE;
    unsigned int subgrid_size     = SUBGRID_SIZE;
    unsigned int integration_time = INTEGRATION_TIME;
    unsigned int nr_repetitions   = NR_REPETITIONS;

    // Derived constants
    unsigned int nr_baselines    = (nr_stations * (nr_stations - 1)) / 2;
    float cell_size              = image_size / grid_size;
    unsigned int kernel_size     = (subgrid_size / 4) + 1;

    // Allocate and initialize data structures
    std::clog << ">>> Initialize data structures" << std::endl;
    idg::Array1D<float> wavenumbers =
        idg::get_example_wavenumbers(nr_channels);
    idg::Array1D<float> frequencies =
        idg::get_example_frequencies(nr_channels);
    idg::Array3D<idg::Visibility<std::complex<float>>> visibilities =
        idg::get_example_visibilities(nr_baselines, nr_timesteps, nr_channels);
    idg::Array1D<std::pair<unsigned int,unsigned int>> baselines =
        idg::get_example_baselines(nr_stations, nr_baselines);
    idg::Array2D<idg::UVWCoordinate<float>> uvw =
        idg::get_example_uvw(nr_stations, nr_baselines, nr_timesteps);
    idg::Array3D<std::complex<float>> grid =
        idg::get_zero_grid(nr_correlations, grid_size, grid_size);
    idg::Array4D<idg::Matrix2x2<std::complex<float>>> aterms =
        idg::get_example_aterms(nr_timeslots, nr_stations, subgrid_size, subgrid_size);
    idg::Array1D<unsigned int> aterms_offsets =
        idg::get_example_aterms_offsets(nr_timeslots, nr_timesteps);
    idg::Array2D<float> spheroidal =
        idg::get_identity_spheroidal(subgrid_size, subgrid_size);

    // Create plan
    std::clog << ">>> Create plan" << std::endl;
    idg::Plan plan(
        kernel_size, subgrid_size, grid_size, cell_size,
        frequencies, uvw, baselines, aterms_offsets);
    auto total_nr_subgrids     = plan.get_nr_subgrids();
    auto total_nr_timesteps    = plan.get_nr_timesteps();
    auto total_nr_visibilities = plan.get_nr_visibilities();
    auto metadata              = plan.get_metadata_ptr();

    // Allocate subgrids
    idg::Array4D<std::complex<float>> subgrids(total_nr_subgrids, nr_correlations, subgrid_size, subgrid_size);

    // Run kernel
    std::clog << ">>> Run kernel " << std::endl;
    auto total_runtime = -omp_get_wtime();
    auto min_runtime  = 0;

    for (int i = 0; i < nr_repetitions; i++) {
        auto runtime = -omp_get_wtime();

        kernel_gridder(
                total_nr_subgrids,
                grid_size,
                image_size,
                w_offset,
                nr_channels,
                nr_stations,
                uvw.data(),
                wavenumbers.data(),
                (std::complex<float>*) visibilities.data(),
                spheroidal.data(),
                (std::complex<float>*) aterms.data(),
                metadata,
                subgrids.data());

        runtime += omp_get_wtime();
        min_runtime = i == 0 ? runtime : fmin(min_runtime, runtime);
    }

    // Report performance
    total_runtime += omp_get_wtime();
    auto avg_runtime = total_runtime/nr_repetitions;
    auto flops = flops_gridder(nr_channels, total_nr_timesteps, total_nr_subgrids);
    auto gflops = flops * 1e-9 / min_runtime;
    auto mvis = total_nr_visibilities * 1e-6 / min_runtime;
    printf("Avg. runtime:     %.2f\n", avg_runtime);
    printf("Min. runtime:     %.2f\n", min_runtime);
    printf("GFlop/s:          %.2f\n", gflops);
    printf("Mvisibilities:    %.2f\n", mvis);

    return EXIT_SUCCESS;
}
