# Introduction
This is the reference code for the IDG gridder kernel, including an example 
program that demonstrates how to use it.

# Usage
```
make
./main.x
```

# Legal
ASTRON has a patent pending on the Image Domain Gridding method implemented by
this software.  This software is licensed under the GNU General Public License
v3.0 (GPLv3.0).  That means that you are allowed to use, modify, distribute and 
sell this software under the conditions of GPLv3.0. For the exact terms of
GPLv3.0, see the LICENSE file.  Making, using, distributing, or offering for 
sale of independent implementations of the Image Domain Gridding method, other
than under GPLv3.0, might infringe upon patent claims by ASTRON. In that case
you must obtain a license from ASTRON.

ASTRON (Netherlands Institute for Radio Astronomy)  
Attn. IPR Department  
P.O.Box 2, 7990 AA Dwingeloo, The Netherlands  
T +31 521 595100