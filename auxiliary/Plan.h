/*******************************************************************************
 *
 * Plan.h
 *
 * Copyright (C) 2017
 * ASTRON (Netherlands Institute for Radio Astronomy)
 * P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
 *
 * This file is part of the Image Domain Gridding (IDG) suite.
 * The  IDG suite is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * The IDG suite is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with the IDG suite. If not, see <http://www.gnu.org/licenses/>.
 *
 ******************************************************************************/

#ifndef IDG_PLAN_H_
#define IDG_PLAN_H_

#include <vector>
#include <limits>
#include <stdexcept> // invalid_argument
#include <cmath>
#include <numeric>
#include <omp.h>

#include "Types.h"


namespace idg {

        class Plan {

        public:
            // Constructors
            Plan() {};

            Plan(
                const int kernel_size,
                const int subgrid_size,
                const int grid_size,
                const float cell_size,
                const Array1D<float>& frequencies,
                const Array2D<UVWCoordinate<float>>& uvw,
                const Array1D<std::pair<unsigned int,unsigned int>>& baselines,
                const Array1D<unsigned int>& aterms_offsets,
                const int max_nr_timesteps_per_subgrid = std::numeric_limits<int>::max());

            // Destructor
            virtual ~Plan() = default;

            void initialize(
                const int kernel_size,
                const int subgrid_size,
                const int grid_size,
                const float cell_size,
                const Array1D<float>& frequencies,
                const Array2D<UVWCoordinate<float>>& uvw,
                const Array1D<std::pair<unsigned int,unsigned int>>& baselines,
                const Array1D<unsigned int>& aterms_offsets,
                const int max_nr_timesteps_per_subgrid = std::numeric_limits<int>::max());

            // total number of subgrids
            int get_nr_subgrids() const;

            // number of subgrids one baseline
            int get_nr_subgrids(int baseline) const;

            // number of subgrids for baselines b1 to b1+n-1
            int get_nr_subgrids(int baseline, int n) const;

            // returns index of first index of baseline
            int get_subgrid_offset(int baseline) const;

            // max number of subgrids for n baselines between bl1 and bl2+n
            int get_max_nr_subgrids(int bl1, int bl2, int n) const;

            // max number of subgrids for 1 baseline
            int get_max_nr_subgrids() const;

            // total number of timesteps
            int get_nr_timesteps() const;

            // number of timesteps one baseline
            int get_nr_timesteps(int baseline) const;

            // number of timesteps for baselines b1 to b1+n-1
            int get_nr_timesteps(int baseline, int n) const;

            // total number of visibilities
            int get_nr_visibilities() const;

            // number of visibilities one baseline
            int get_nr_visibilities(int baseline) const;

            // number of visibilities for baselines b1 to b1+n-1
            int get_nr_visibilities(int baseline, int n) const;

            // number of baselines
            int get_nr_baselines() const {
                return total_nr_timesteps_per_baseline.size();
            }

            const Metadata* get_metadata_ptr(int baseline = 0) const;

            void copy_metadata(void *ptr) const;

        private:
            std::vector<Metadata> metadata;
            std::vector<int> subgrid_offset;
            std::vector<int> total_nr_timesteps_per_baseline;
            std::vector<int> total_nr_visibilities_per_baseline;

        }; // class Plan

} // namespace idg

#endif
