/*******************************************************************************
 *
 * Init.cpp
 *
 * Copyright (C) 2017
 * ASTRON (Netherlands Institute for Radio Astronomy)
 * P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
 *
 * This file is part of the Image Domain Gridding (IDG) suite.
 * The  IDG suite is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * The IDG suite is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with the IDG suite. If not, see <http://www.gnu.org/licenses/>.
 *
 ******************************************************************************/

#include "Init.h"

namespace idg {

    const std::string ENV_LAYOUT_FILE  = "LAYOUT_FILE";

    void init_example_uvw(
        void *ptr,
        int nr_stations,
        int nr_baselines,
        int nr_time,
        float integration_time)
    {
        typedef struct { float u, v, w; } UVW;
        typedef UVW UVWType[nr_baselines][nr_time];

        UVWType *uvw = (UVWType *) ptr;

        // Try to load layout file from environment
        char *cstr_layout_file = getenv(ENV_LAYOUT_FILE.c_str());

        // Check whether layout file exists
        char filename[512];
        if (cstr_layout_file) {
            sprintf(filename, "%s/%s", LAYOUT_DIR, cstr_layout_file);
        } else {
            sprintf(filename, "%s/%s", LAYOUT_DIR, LAYOUT_FILE);
        }

        if (!uvwsim_file_exists(filename)) {
            std::cerr << "Unable to find specified layout file: "
                      << filename << std::endl;
            exit(EXIT_FAILURE);
        }

        // Read the number of stations in the layout file.
        int nr_stations_file = uvwsim_get_num_stations(filename);

        // Check wheter the requested number of station is feasible
        if (nr_stations_file < nr_stations) {
           std::cerr << "More stations requested than present in layout file: "
                     << "(" << nr_stations_file << ")" << std::endl;
            exit(EXIT_FAILURE);
        }

        // Allocate memory for antenna coordinates
        double *x = (double*) malloc(nr_stations_file * sizeof(double));
        double *y = (double*) malloc(nr_stations_file * sizeof(double));
        double *z = (double*) malloc(nr_stations_file * sizeof(double));

        // Load the antenna coordinates
        #if defined(DEBUG)
        printf("looking for stations file in: %s\n", filename);
        #endif

        if (uvwsim_load_station_coords(filename, nr_stations_file, x, y, z) != nr_stations_file) {
            std::cerr << "Failed to read antenna coordinates." << std::endl;
            exit(EXIT_FAILURE);
        }

        // Select some antennas randomly when not all antennas are requested
        if (nr_stations < nr_stations_file) {
            // Allocate memory for selection of antenna coordinates
            double *_x = (double*) malloc(nr_stations * sizeof(double));
            double *_y = (double*) malloc(nr_stations * sizeof(double));
            double *_z = (double*) malloc(nr_stations * sizeof(double));

            // Generate nr_stations random numbers
            int station_number[nr_stations];
            int i = 0;
            srandom(RANDOM_SEED);
            while (i < nr_stations) {
                int index = nr_stations_file * ((double) random() / RAND_MAX);
                bool found = true;
                for (int j = 0; j < i; j++) {
                    if (station_number[j] == index) {
                        found = false;
                        break;
                    }
                }
                if (found) {
                     station_number[i++] = index;
                }
             }

            // Set stations
            for (int i = 0; i < nr_stations; i++) {
                _x[i] = x[station_number[i]];
                _y[i] = y[station_number[i]];
                _z[i] = z[station_number[i]];
            }

            // Swap pointers and free memory
            double *__x = x;
            double *__y = y;
            double *__z = z;
            x = _x;
            y = _y;
            z = _z;
            free(__x);
            free(__y);
            free(__z);
        }

        // Define observation parameters
        double ra0  = RIGHT_ASCENSION;
        double dec0 = DECLINATION;
        double start_time_mjd = uvwsim_datetime_to_mjd(YEAR, MONTH, DAY, HOUR, MINUTE, SECONDS);
        double obs_length_hours = (nr_time * integration_time) / (3600.0);
        double obs_length_days = obs_length_hours / 24.0;

        // Allocate memory for baseline coordinates
        int nr_coordinates = nr_time * nr_baselines;
        double *uu = (double*) malloc(nr_coordinates * sizeof(double));
        double *vv = (double*) malloc(nr_coordinates * sizeof(double));
        double *ww = (double*) malloc(nr_coordinates * sizeof(double));

        // Evaluate baseline uvw coordinates.
        #pragma omp parallel for
        for (int t = 0; t < nr_time; t++) {
            double time_mjd = start_time_mjd + t
                              * (obs_length_days/(double)nr_time);
            size_t offset = t * nr_baselines;
            uvwsim_evaluate_baseline_uvw(
                &uu[offset], &vv[offset], &ww[offset],
                nr_stations, x, y, z, ra0, dec0, time_mjd);
        }

        // Fill UVW datastructure
        #pragma omp parallel for
        for (int bl = 0; bl < nr_baselines; bl++) {
            for (int t = 0; t < nr_time; t++) {
                int i = t * nr_baselines + bl;
                UVW value = {(float) uu[i], (float) vv[i], (float) ww[i]};
                (*uvw)[bl][t] = value;
            }
        }

        // Free memory
        free(x); free(y); free(z);
        free(uu); free(vv); free(ww);
    }

    Array1D<float> get_example_wavenumbers(
        unsigned int nr_channels,
        float start_frequency,
        float frequency_increment
    ) {
        Array1D<float> wavenumbers(nr_channels);

        for (int chan = 0; chan < nr_channels; chan++) {
            float frequency = start_frequency + frequency_increment * chan;
            wavenumbers(chan) =  2 * M_PI * frequency / SPEED_OF_LIGHT;
        }

        return wavenumbers;
    }

    Array1D<float> get_example_frequencies(
        unsigned int nr_channels,
        float start_frequency,
        float frequency_increment
    ) {
        Array1D<float> frequencies(nr_channels);

        for (int chan = 0; chan < nr_channels; chan++) {
            frequencies(chan) = start_frequency + frequency_increment * chan;
        }

        return frequencies;
    }

    Array3D<Visibility<std::complex<float>>> get_example_visibilities(
        unsigned int nr_baselines,
        unsigned int nr_timesteps,
        unsigned int nr_channels)
    {
        Array3D<Visibility<std::complex<float>>> visibilities(nr_baselines, nr_timesteps, nr_channels);
        const Visibility<std::complex<float>> visibility = {1.0f, 0.0f, 0.0f, 1.0f};

        // Set all visibilities
        for (int bl = 0; bl < nr_baselines; bl++) {
            for (int time = 0; time < nr_timesteps; time++) {
                for (int chan = 0; chan < nr_channels; chan++) {
                    visibilities(bl, time, chan) = visibility;
                }
            }
        }

        return visibilities;
    }

    Array1D<std::pair<unsigned int,unsigned int>> get_example_baselines(
        unsigned int nr_stations,
        unsigned int nr_baselines)
    {
        Array1D<std::pair<unsigned int,unsigned int>> baselines(nr_baselines);

        int bl = 0;

        for (int station1 = 0 ; station1 < nr_stations; station1++) {
            for (int station2 = station1 + 1; station2 < nr_stations; station2++) {
                if (bl >= nr_baselines) {
                    break;
                }
                baselines(bl) = std::pair<unsigned int,unsigned int>(station1, station2);
                bl++;
            }
        }

        return baselines;
    }

    Array2D<UVWCoordinate<float>> get_example_uvw(
        unsigned int nr_stations,
        unsigned int nr_baselines,
        unsigned int nr_timesteps,
        float integration_time)
    {
        Array2D<UVWCoordinate<float>> uvw(nr_baselines, nr_timesteps);
        void *uvw_ptr = uvw.data();
        init_example_uvw(uvw_ptr, nr_stations, nr_baselines, nr_timesteps, integration_time);

        return uvw;
    }

    Array3D<std::complex<float>> get_zero_grid(
        unsigned int nr_correlations,
        unsigned int height,
        unsigned int width
    ) {
        Array3D<std::complex<float>> grid(nr_correlations, height, width);
        memset(grid.data(), 0, grid.bytes());
        return grid;
    }

    Array4D<Matrix2x2<std::complex<float>>> get_example_aterms(
        unsigned int nr_timeslots,
        unsigned int nr_stations,
        unsigned int height,
        unsigned int width)
    {
        Array4D<Matrix2x2<std::complex<float>>> aterms(nr_timeslots, nr_stations, height, width);
        const Matrix2x2<std::complex<float>> aterm = {1.0f, 0.0f, 0.0f, 1.0f};

        for (int t = 0; t < nr_timeslots; t++) {
            for (int ant = 0; ant < nr_stations; ant++) {
                for (int y = 0; y < height; y++) {
                    for (int x = 0; x < width; x++) {
                        aterms(t, ant, y, x) = aterm;
                    }
                }
            }
        }

        return aterms;
    }

    Array1D<unsigned int> get_example_aterms_offsets(
        unsigned int nr_timeslots,
        unsigned int nr_timesteps)
    {
        Array1D<unsigned int> aterms_offsets(nr_timeslots + 1);

        for (int time = 0; time < nr_timeslots; time++) {
             aterms_offsets(time) = time * (nr_timesteps / nr_timeslots);
        }

        aterms_offsets(nr_timeslots) = nr_timesteps;

        return aterms_offsets;
    }

    Array2D<float> get_identity_spheroidal(
        unsigned int height,
        unsigned int width)
    {
        Array2D<float> spheroidal(height, width);

        float value = 1.0;

        for (int y = 0; y < height; y++) {
            for (int x = 0; x < width; x++) {
                 spheroidal(y, x) = value;
            }
        }

        return spheroidal;
    }

} // namespace idg
