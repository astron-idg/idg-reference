/*******************************************************************************
 *
 * Constants.h
 *
 * Copyright (C) 2017
 * ASTRON (Netherlands Institute for Radio Astronomy)
 * P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
 *
 * This file is part of the Image Domain Gridding (IDG) suite.
 * The  IDG suite is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * The IDG suite is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with the IDG suite. If not, see <http://www.gnu.org/licenses/>.
 *
 ******************************************************************************/

#pragma once

// Kernel constants
#define NR_CORRELATIONS  4
#define SUBGRID_SIZE     24

// Additional constants
#define W_OFFSET         0
#define NR_STATIONS      50
#define NR_CHANNELS      8
#define NR_TIMESTEPS     2048
#define NR_TIMESLOTS     32
#define IMAGE_SIZE       0.025f
#define GRID_SIZE        1024
#define NR_REPETITIONS   1
