/*******************************************************************************
 *
 * Init.h
 *
 * Copyright (C) 2017
 * ASTRON (Netherlands Institute for Radio Astronomy)
 * P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
 *
 * This file is part of the Image Domain Gridding (IDG) suite.
 * The  IDG suite is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * The IDG suite is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with the IDG suite. If not, see <http://www.gnu.org/licenses/>.
 *
 ******************************************************************************/

#ifndef IDG_INIT_H_
#define IDG_INIT_H_

#include <iostream>
#include <complex>
#include <cstring> // memset

#include "uvwsim.h"
#include "Types.h"

/* Macro */
#define MIN(a,b) (((a)<(b))?(a):(b))
#define MAX(a,b) (((a)>(b))?(a):(b))

/* Constants */
#define RANDOM_SEED         1234
#define SPEED_OF_LIGHT      299792458.0

/* Observation parameters */
static const std::string ENV_LAYOUT_FILE;
#define LAYOUT_DIR          "auxiliary"
#define LAYOUT_FILE         "SKA1_low_ecef"
#define START_FREQUENCY     150e6
#define FREQUENCY_INCREMENT 0.7e6
#define RIGHT_ASCENSION     (10.0 * (M_PI/180.))
#define DECLINATION         (70.0 * (M_PI/180.))
#define YEAR                2014
#define MONTH               03
#define DAY                 20
#define HOUR                01
#define MINUTE              57
#define SECONDS             1.3
#define INTEGRATION_TIME    1.0f

namespace idg {

    Array1D<float> get_example_wavenumbers(
        unsigned int nr_channels,
        float start_frequency = START_FREQUENCY,
        float frequency_increment = FREQUENCY_INCREMENT);
    
    Array1D<float> get_example_frequencies(
        unsigned int nr_channels,
        float start_frequency = START_FREQUENCY,
        float frequency_increment = FREQUENCY_INCREMENT);
    
    Array3D<Visibility<std::complex<float>>> get_example_visibilities(
        unsigned int nr_stations,
        unsigned int nr_timesteps,
        unsigned int nr_channels);
    
    Array1D<std::pair<unsigned int,unsigned int>> get_example_baselines(
        unsigned int nr_stations,
        unsigned int nr_baselines);
    
    Array2D<UVWCoordinate<float>> get_example_uvw(
        unsigned int nr_stations,
        unsigned int nr_baselines,
        unsigned int nr_timesteps,
        float integration_time = INTEGRATION_TIME);
    
    Array3D<std::complex<float>> get_zero_grid(
        unsigned int nr_correlations,
        unsigned int height,
        unsigned int width);
    
    idg::Array4D<Matrix2x2<std::complex<float>>> get_example_aterms(
        unsigned int nr_timeslots,
        unsigned int nr_stations,
        unsigned int height,
        unsigned int width);
    
    Array1D<unsigned int> get_example_aterms_offsets(
        unsigned int nr_timeslots,
        unsigned int nr_timesteps);
    
    Array2D<float> get_identity_spheroidal(
        unsigned int height,
        unsigned int width);
    
    idg::Array2D<float> get_example_spheroidal(
        unsigned int height,
        unsigned int width);

} // namespace idg

#endif
